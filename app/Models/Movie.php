<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    use HasFactory;

    public $timestamps = false;
    public $fillable = [
        "name",
        "description",
        "release_date",
        "note",
        "image_path"
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'categories_movies', "movie_id", "category_id");
    }
}
