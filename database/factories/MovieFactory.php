<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Movie>
 */
class MovieFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $images = [
            'movie1.jpg',
            'movie2.jpg',
            'movie3.jpg',
        ];

        return [
            "uuid" => fake()->uuid(),
            "name" => fake()->unique()->company(),
            "description" => fake()->text(),
            "release_date" => fake()->date(),
            "note" => fake()->numberBetween(0,5),
            "image_path" => 'public/movies/' . fake()->randomElement($images),
        ];
    }
}
