<?php

use App\Http\Controllers\MovieController;
use App\Http\Controllers\CategoryController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/movies/{output_format}/{searchTitle?}/{searchDescription?}', [MovieController::class, 'index']);
Route::get('/movie/{movie}/{output_format}', [MovieController::class, 'show']);
Route::post('/movie/{output_format}', [MovieController::class, 'store']);
Route::patch('/movie/{movie}/{output_format}', [MovieController::class, 'update']);
Route::delete('/movie/{movie}', [MovieController::class, 'destroy']);

Route::get('/categories/{output_format}', [CategoryController::class, 'index']);
Route::get('/category/{category}/{output_format}', [CategoryController::class, 'show']);
Route::post('/category/{output_format}', [CategoryController::class, 'store']);
Route::patch('/category/{category}/{output_format}', [CategoryController::class, 'update']);
Route::delete('/category/{category}', [CategoryController::class, 'destroy']);

Route::get('/movies/{filename}', [MovieController::class, 'getImage'])->where('filename', '^[^/]+$');
