<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Spatie\ArrayToXml\ArrayToXml;


class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param string $output_format
     * @param string|null $searchTitle
     * @param string|null $searchDescription
     * @return mixed Collection of movies
     */
    public function index(string $output_format, string $searchTitle = null, string $searchDescription = null): mixed
    {

        $movieByTitle = Movie::where("name", $searchTitle)->get();
        $movieByDescription = Movie::where("description", $searchDescription)->get();

        if(!$movieByTitle || !$movieByDescription){
            return response("Données non trouvées", 404);
        }

        $movies = Movie::query()->with('categories')
            ->when($searchTitle, function ($query, $search) {
                $query->where('name', 'like', '%' . $search . '%');
            })
            ->when($searchDescription, function ($query, $search) {
                $query->where('description', 'like', '%' . $search . '%');
            })->paginate(10);

            if ($movies->isEmpty()) {
                return response()->json(['error' => 'Aucun résultat trouvé'], 404);
            }

        switch ($output_format) {
            case 'json':
                return response()->json($movies, 200);

            case 'xml':
                $xml = new \SimpleXMLElement('<movies/>');
                foreach ($movies as $movie) {
                    $movieXml = $xml->addChild('movie');
                    $movieXml->addChild('id', $movie->id);
                    $movieXml->addChild('name', $movie->name);
                    $movieXml->addChild('description', $movie->description, 'UTF-8');
                    $movieXml->addChild('release_date', $movie->release_date);
                    $movieXml->addChild('note', $movie->note);
                    $movieXml->addChild('image_url', asset('/movies/' . basename($movie->image_path)));
                }

                $response = new HttpResponse($xml->asXML(), 200);
                $response->header('Content-Type', 'application/xml; charset=utf-8');

                return $response;

            default:
                return response("Ce format n'est pas pris en charge", 400);
        }
    }

    /**
     * Affiche l'image du film
     * @param $filename
     * @return mixed
     */
    public function getImage ($filename): mixed
    {
        if (!Storage::disk('public')->exists('movies/' . $filename)) {
            return response("Ce fichier n'existe pas", 404);
        }

        $path = Storage::disk('public')->get('movies/' . $filename);
        $type = Storage::disk('public')->mimeType('movies/' . $filename);

        return response($path, 200)->header('Content-Type', $type);
    }

    /**
     * Display the specified resource.
     *
     * @param string $output_format
     * @param Movie $movie
     * @return mixed
     */
    public function show(Movie $movie, string $output_format): mixed
    {
        if (!Movie::find($movie)) {
            return response("Ce film n'a pas été trouvé", 404);
        }

        switch ($output_format) {
            case 'json':
                return new JsonResponse($movie->load("categories"), 200);


            case 'xml':
                $data = $movie->toArray();
                $data['image_url'] = Storage::disk('public')->url('movies/' . basename($movie->image_path));
                $data['categories'] = $movie->categories->toArray();
                $result = new ArrayToXml($data, [], true, 'UTF-8');
                return response($result->toXml(), 200);


            default:
                return response("Ce format n'est pas pris en charge", 400);

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request, string $output_format): mixed
    {
        $validated = Validator::make(
            $request->all(),
            [
                'name' => 'required | max:128',
                'description' => 'required | max:2048',
                'release_date' => 'required | date_format:Y-m-d',
                'notes' => 'nullable | integer | min:0 | max:5',
                'image_path' => 'required | image | mimes:jpeg,png,jpg,gif,svg | max:2048'
            ]
        );

        if ($validated->fails()) {
            return new JsonResponse(['errors' => $validated->errors()], 422);
        }

        try {
            if ($request->hasFile('image_path')) {
                $file = $request->file('image_path');
                $filename = $file->getClientOriginalName();
                $path = $file->storeAs('movies', $filename, 'public');
            }
        } catch (\Exception $e) {
            return response("Une erreur est survenue lors de l'upload de l'image", 500);
        }

        $movie = Movie::create($request->all() + ['image_path' => $path]);

        switch ($output_format) {
            case 'json':
                return new JsonResponse($movie, 201);


            case 'xml':
                $data = $movie->toArray();
                $data['image_url'] = Storage::disk('public')->url($movie->image_path);
                $data['categories'] = $movie->categories->toArray();
                $result = new ArrayToXml($data, [], true, 'UTF-8');
                return response($result->toXml(), 201);


            default:
                return response("Ce format n'est pas pris en charge", 400);

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Movie $movie
     *
     * @return mixed
     */
    public function update(Request $request, Movie $movie, string $output_format): mixed
    {
        if (!Movie::find($movie)) {
            return response("Ce film n'a pas été trouvé", 404);
        }

        $validated = Validator::make(
            $request->all(),
            [
                'name' => 'required | max:128',
                'description' => 'required | max:2048',
                'release_date' => 'required | date_format:Y-m-d',
                'notes' => 'nullable | integer | min:0 | max:5',
                'image_path' => 'required | image | mimes:jpeg,png,jpg,gif,svg | max:2048'
            ]
        );

        if ($validated->fails()) {
            return new JsonResponse(['errors' => $validated->errors()], 422);
        }

        if ($request->hasFile('image_path')) {
            try {
                $path = $request->file('image_path')->store('public/movies');
            } catch (\Exception $e) {
                return response("Une erreur est survenue lors de l'upload de l'image", 500);
            }

            $movie->update($request->all() + ['image_path' => $path]);
        }

        $movie->update($request->all());


        switch ($output_format) {
            case 'json':
                return new JsonResponse($movie, 201);


            case 'xml':
                $data = $movie->toArray();
                $data['image_url'] = asset('/movies/' . basename($movie->image_path));
                $data['categories'] = $movie->categories->toArray();
                $result = new ArrayToXml($data, [], true, 'UTF-8');
                return response($result->toXml(), 201);


            default:
                return response("Ce format n'est pas pris en charge", 400);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Movie $movie
     *
     * @return ResponseFactory
     */
    public function destroy(Movie $movie): \Illuminate\Http\Response|\Illuminate\Contracts\Routing\ResponseFactory
    {
        if (!Movie::find($movie)) {
            return response("Ce film n'a pas été trouvé", 404);
        }

        $movie->delete();

        return response('Film supprimé', 200);
    }
}
